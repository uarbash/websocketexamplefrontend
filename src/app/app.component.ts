import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {WebSocketSubject} from "rxjs/webSocket";

interface Counter{
  id: number,
  counter: number
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app';
  public counter = 0;
  private websocket: WebSocket | undefined;
  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    const protocol = window.location.protocol.replace('http', 'ws');
    const host = window.location.host;
    this.websocket = new WebSocket('ws://localhost:8080/websocket/counter');

    this.http.get<Counter>('/api/v1/counter/get/1', {}).subscribe((value: Counter) => {
      if (!value){
        this.http.post<Counter>('/api/v1/counter/set/1', {}).subscribe((newValue: Counter) => {
          this.counter = newValue.counter;
        });
      }
      if (value){
        this.counter = value.counter;
      }
    });
    this.websocket.onopen = (event) => {
      console.info(event.timeStamp)
    }
    this.websocket.onclose = (event) => {
      console.info(event.timeStamp);
    }
    this.websocket.onmessage = (message) => {
      console.log(message.data);
    }
    console.log(this.websocket.url)
  }

  increaseValue(): void{
    this.http.post<Counter>('/api/v1/counter/increase/1', {}).subscribe(value => {
      this.counter = value.counter
    });
    console.log(this.websocket)
  }

  decreaseValue(): void{
    this.http.post<Counter>('/api/v1/counter/decrease/1', {}).subscribe(value => {
      this.counter = value.counter
    });
  }
}
